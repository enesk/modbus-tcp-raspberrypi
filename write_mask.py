# write_mask.py
# This function writes to a single bit in the input register 

from __future__ import print_function

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp, hooks


class TcpMasterEnerwhere(modbus_tcp.TcpMaster):

	def mask_write(self, slave_id, register_address, bit_address, bit_value):
		"""
		Writes to a single bit of a holding register. The function reads in the value	
		from the register, changes the required bit and writes back to the register.
		Please note that this method is not recommendable if there is a chance that the
		register value changes between reading and writing.
		
		slave_id is the modbus id of the slave
		bit_address is the location of the bit to be changed starting at the LSB starting from 0
		bit_value value to be added at the bit address
		"""
		if bit_address not in range(15):
			raise 'Bit address out of range.'

		elif bit_value not in [0, 1]:
			raise 'Bit value can be either 0 or 1.'
			
		value = self.execute(slave_id, cst.READ_HOLDING_REGISTERS, register_address, 1)[0] # Pull the register value

		binary_value = bin(value)[2:]
		parsed_binary_value = [b for b in binary_value]
		parsed_binary_value[len(binary_value)-1-bit_address] = str(bit_value) # Change the required bit
		binary_value2 = ''.join(parsed_binary_value)	# Update the binary value

		self.execute(slave_id, cst.WRITE_SINGLE_REGISTER, \
			register_address, output_value=int(binary_value2, base = 2)) # Write back to the register


def main():
	"""
	main

	Testing the method mask_write()
	"""
	logger = modbus_tk.utils.create_logger("console")

	def on_after_recv(data):
	    master, bytes_data = data
	    #print(bytes_data)

	hooks.install_hook('modbus.Master.after_recv', on_after_recv)

	try:

		def on_before_connect(args):
			master = args[0]
			print("on_before_connect", master._host, master._port)

		hooks.install_hook("modbus_tcp.TcpMaster.before_connect", on_before_connect)

		def on_after_recv(args):
			response = args[1]
			#print("on_after_recv", len(response), "bytes received")

		hooks.install_hook("modbus_tcp.TcpMaster.after_recv", on_after_recv)

		# Connect to the slave
		master = TcpMasterEnerwhere(host = "127.0.0.1")
		master.set_timeout(5)
		logger.info("connected")

		# Print the value before editing
		bit_address = 2
		bit_value = 0
		slave_id = 1
		register_address = 0

		value = master.execute(slave_id, cst.READ_HOLDING_REGISTERS, register_address, 1)[0]
		print('Changing the bit of order', bit_address, 'to', bit_value,
			'in register', register_address, 'of slave '+str(slave_id)+'.')
		print('Value before editting', bin(value)[2:])

		# Edit the value
		master.mask_write(1, 0, bit_address, bit_value)

		# Print the value after editing
		value = master.execute(slave_id, cst.READ_HOLDING_REGISTERS, register_address, 1)[0]
		print('Value after editting', bin(value)[2:])

	except modbus_tk.modbus.ModbusError as exc:
	    logger.error("%s- Code=%d", exc, exc.get_exception_code())

if __name__ == "__main__":
    main()
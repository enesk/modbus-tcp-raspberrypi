# README #

### The repository contains several Python scripts ###

* `slave.py` and `master.py` can be used to test the functionality of modbus_tk library
* `fake_slaves.py` is a code written to emulate the functionality of Tesla SMC and PC SunGrow inverter.
* `write_mask` implements a function that enables writing to a single bit of a holding register.

### Setting up ###

The only dependency of this code is the [modbus-tk](https://github.com/ljean/modbus-tk) library.
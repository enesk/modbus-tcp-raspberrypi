import csv
import time
import datetime
import sys
import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--ip-address', type = str, help='The IP address of the device.')

args = parser.parse_args()

ip_addresses =  {"192.168.0.100": 'Tesla SMC' ,  "192.168.0.101": 'Tesla SMC', "192.168.0.60"  : "PV Inverter - SunGrow",
"192.168.0.70" : "DG Controller"}


# Mapping to time stamps
time_stamps = ['12:00:00 AM', '12:01:00 AM', '12:02:00 AM', '12:03:00 AM', \
	'12:04:00 AM', '12:05:00 AM', '12:06:00 AM']

slave_addresses = {'Tesla SMC' : [1], "PV Inverter - SunGrow" : [11],  "DG Controller" : [1, 2, 3]}
size_mapping = {'int32' : 2, 'int16' : 1, 'uint16' : 1, 'enum': 1}
type_mapping = {'R' :  cst.ANALOG_INPUTS, 'R/W' : cst.HOLDING_REGISTERS}

deviceName = ip_addresses[args.ip_address]
print('Device is '+deviceName+'.')

class Device:
	def __init__(self, deviceName, ip_address):
		self.deviceName = deviceName
		self.csvName = "csv/fake-data.csv"
		self.ip_address = ip_address
	
	def start(self):
		self.parameters = self.defineParameters()
		self.fakeValues = self.getFakeValues()
		self.propertiesDictionary = self.getPropertiesDictionary() # A dictionary of tuples: 0:size, 1:address, 2:R/W

		slave_ids_column = []
		for key in self.propertiesDictionary.keys():
			if self.deviceName in key:
				slave_ids_column.append(self.propertiesDictionary[key][3])

		self.slave_ids = list(set(slave_ids_column))

		# The blocks are grouped based on register types
		self.block_addresses = {}
		self.block_names = {}
		for slave_id in self.slave_ids:
			self.block_addresses[slave_id] = {}

			rw_column = []
			for key in self.propertiesDictionary.keys():
				if self.deviceName in key and self.propertiesDictionary[key][3] == slave_id:
					rw_column.append(self.propertiesDictionary[key][2])

			self.block_names[slave_id] = list(set(rw_column))

			for block_name in self.block_names[slave_id]:

				addresses = []
				for key in self.propertiesDictionary.keys():
					if self.propertiesDictionary[key][2] == block_name and \
							 self.propertiesDictionary[key][1] != 'Register Address' and \
							 	self.deviceName in key and \
							 	self.propertiesDictionary[key][3] == slave_id:
						addresses.append(int(self.propertiesDictionary[key][1]))

				self.block_addresses[slave_id][block_name] = {'start' : (min(addresses)-1), 'end' : (max(addresses)+1)}

		self.logger = self.instantiateLogger()
		self.server = self.instantiateServer()
		self.slaves = self.instantiateSlaves()

	def stop(self):
		self.server.stop()

	def defineParameters(self):
	# Define the paramters for this device
		with open(self.csvName, 'rb') as csvfile:
			reader = csv.reader(csvfile)
			rows = []
			for row in reader:
				rows.append(row)
			headers = rows.pop(0) # extract the headers
			headers = [header for header in headers if self.deviceName in header]
			return headers

	def getFakeValues(self):
	# Load the fake values
		with open(self.csvName, 'rb') as csvfile:
			reader = csv.reader(csvfile)
			rows = []
			for row in reader:
				rows.append(row)

			# Create an empty dictionary that stores the values for each device
			exampleValues = {}    
			headers = rows.pop(0) # extract the headers
			headers.pop(0)
			for header in headers:
				exampleValues[header] = {}

			# Add the values to the dictionary
			for row in rows:
				for i in range(len(headers)):
					exampleValues[headers[i]][row[0]] = int(row[i+1])
			return exampleValues

	def getPropertiesDictionary(self):
	# Get the properties for each parameter
		with open("csv/RegisterList.csv","rb") as source:
			rdr= csv.reader( source )
			with open("csv/RegisterListMain.csv","wb") as result:
		    		wtr= csv.writer( result )
		    		for r in rdr:
					wtr.writerow((r[2], r[4], r[5], r[6], r[9], r[10]))

		with open('csv/RegisterListMain.csv', mode='r') as infile:
			reader = csv.reader(infile)
			mydict = {rows[0]:tuple(rows[1:]) for rows in reader}
		return mydict

	def instantiateLogger(self):
		logger = modbus_tk.utils.create_logger(name="console", record_format="%(message)s")
		return logger

	def instantiateServer(self):
	#  Create a TCP server	
		server = modbus_tcp.TcpServer(address = self.ip_address)  # Hardcoded for now
		self.logger.info("running...")
		self.logger.info("enter 'quit' for closing the server")
		server.start()
		return server

	def instantiateSlaves(self):
		# Create the slaves
		slaves = []
		for slave_id in self.slave_ids:
			slave = self.server.add_slave(int(slave_id))

			# Instantiate blocks
			for blockName in self.block_names[slave_id]:
					blockAddress = self.block_addresses[slave_id][blockName]['start']
					blockSize= self.block_addresses[slave_id][blockName]['end'] - self.block_addresses[slave_id][blockName]['start']
					blockType = type_mapping[blockName]
					slave.add_block(blockName, blockType, blockAddress-1, blockSize)

			slaves.append(slave)

		return slaves

	def run(self):
		stepSize = 1  #  Determines the update rate. In minutes.
		dateTime = datetime.datetime.strptime(time_stamps[0] ,'%I:%M:%S %p')
		truncatedMinutes = (dateTime.minute // stepSize)  * stepSize	
		truncatedCurrentTime = "anything"

		while(1):
			# Update the required registers every step size. The time is truncated
			# depending on the CSV file
			truncatedMinutes = (dateTime.minute // stepSize)  * stepSize
			truncatedDateTime = dateTime.replace(minute = truncatedMinutes)

			# If the truncated time has changed change the CSV file
			if(truncatedDateTime != truncatedCurrentTime):
				truncatedCurrentTime = truncatedDateTime
				formattedTime = truncatedCurrentTime.replace(second = 0).strftime('%I:%M:%S %p')
				for parameter in self.parameters:
					blockName = self.propertiesDictionary[parameter][2]
					registerAddress = int(self.propertiesDictionary[parameter][1]) - 1
					registerSize = size_mapping[self.propertiesDictionary[parameter][0]]
					blockType = type_mapping[self.propertiesDictionary[parameter][2]]
					value = self.fakeValues[parameter][formattedTime]
					blockSlave = int(self.propertiesDictionary[parameter][3])
					if registerSize == 2:
						# If the register is 32bit parse the value into two registers
						binaryValue = [int(bin(value)[2:].zfill(32)[0:16], 2), int(bin(value)[2:].zfill(32)[16:32], 2)]
						for slave in self.slaves:
							if slave._id == blockSlave:
								slave.set_values(blockName, registerAddress, binaryValue)
					else:
						# Else
						binaryValue = int(bin(value)[2:].zfill(32)[16:32], 2)	
						for slave in self.slaves:
							if slave._id == blockSlave:				
								slave.set_values(blockName, registerAddress, binaryValue)
			
			index = raw_input("Enter the row number (1-7): ")
			
			try:
				index = int(index)
				dateTime = datetime.datetime.strptime(time_stamps[index-1] ,'%I:%M:%S %p')

			except ValueError:
				print("The date format is not correct.")



device = Device(deviceName, args.ip_address)


try:
	device.start()
	device.run()
finally:
	device.stop()

#!/bin/sh
IPADDR=$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)

# If there is no LAN connection switch to WLAN port
if [[ -z "${IPADDR// }" ]]
	then
		IPADDR=$(ifconfig wlan0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
		echo $IPADDR
fi


sudo python fake-slaves.py --ip-address $IPADDR